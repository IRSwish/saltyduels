let gsheetdata = null;

getGoogleSheetData();

function getCellData(cell) {
	const cellContent = gsheetdata.feed.entry.find((entry) => {
    return entry.title.$t == cell
  });
	return cellContent.content.$t;
}

function getGoogleSheetData(){
	fetch('https://spreadsheets.google.com/feeds/cells/1-9SRClVJkLZ6Y-BgyN4YGcY843gjvtdrHUtBoQU0_NI/2/public/basic?alt=json')
	.then(function(res) {
		return res.json();
  })
  .then((data) => {
    gsheetdata = data;
	document.querySelector('.malus').innerText = getCellData('A26') + " " + getCellData('B26');
	document.querySelector('.bluename').innerText = getCellData('B1');
	document.querySelector('.orangename').innerText = getCellData('C1');
	if (getCellData('B5') != "-") {
		document.querySelector('.banatkblue').style.backgroundImage = "url(../../GFX/Icons/" + getCellData('B5') + ".png)";
		document.querySelector('.banatkblue').style.animation = "banreveal 2s forwards";
	} else {
		document.querySelector('.banatkblue').style.animation = "none";
		document.querySelector('.banatkblue').style.animation = "banhide 2s forwards";
	}
	if (getCellData('B6') != "-") {
		document.querySelector('.bandefblue').style.backgroundImage = "url(../../GFX/Icons/" + getCellData('B6') + ".png)";
		document.querySelector('.bandefblue').style.animation = "banreveal 2s forwards";
	} else {
		document.querySelector('.bandefblue').style.animation = "none";
		document.querySelector('.bandefblue').style.animation = "banhide 2s forwards";
	}
	if (getCellData('C5') != "-") {
		document.querySelector('.banatkorange').style.backgroundImage = "url(../../GFX/Icons/" + getCellData('C5') + ".png)";
		document.querySelector('.banatkorange').style.animation = "banreveal 2s forwards";
	} else {
		document.querySelector('.banatkorange').style.animation = "none";
		document.querySelector('.banatkorange').style.animation = "banhide 2s forwards";
	}
	if (getCellData('C6') != "-") {
		document.querySelector('.bandeforange').style.backgroundImage = "url(../../GFX/Icons/" + getCellData('C6') + ".png)";
		document.querySelector('.bandeforange').style.animation = "banreveal 2s forwards";
	} else {
		document.querySelector('.bandeforange').style.animation = "none";
		document.querySelector('.bandeforange').style.animation = "banhide 2s forwards";
	}
	
	document.querySelector('.streamerslist').innerHTML = getCellData('F1') + "<br />" + getCellData('G1') + "<br />" + getCellData('F2') + "<br /> " + getCellData('G2');
	
	
	if (getCellData('G3') == "FALSE") {
			document.querySelector('.streamers').style.animation = "opacityhide 2s forwards";
			document.querySelector('.streamerslist').style.animation = "opacityhide 2s forwards";
	}
	else {
		document.querySelector('.streamers').style.visibility = "visible";
		document.querySelector('.streamerslist').style.visibility = "visible";
		document.querySelector('.streamers').style.animation = "opacityreveal 2s forwards";
		document.querySelector('.streamerslist').style.animation = "opacityreveal 2s forwards";
	}
  })
}


setInterval(getGoogleSheetData, 2000);

