const TEMPS_VISIBLE = 5000;

let results = [];

function loadTxt(file) {
  const req = new XMLHttpRequest();
  req.open('GET', file, false);
  req.send(null);
  if (req.status === 200 || req.status === 0) {
    results = JSON.parse(req.responseText);
    setContent();
  }
}
getGoogleSheetData();

let gsheetdata = null;
function getCellData(cell) {
	const cellContent = gsheetdata.feed.entry.find((entry) => {
    return entry.title.$t == cell
  });
	return cellContent.content.$t;
}

function getGoogleSheetData(){
	fetch('https://spreadsheets.google.com/feeds/cells/1-9SRClVJkLZ6Y-BgyN4YGcY843gjvtdrHUtBoQU0_NI/7/public/basic?alt=json')
	.then(function(res) {
		return res.json();
  })
  .then((data) => {
    gsheetdata = data;
    loadTxt('Operators.json'); // lance le json local comme "normalement"
	document.querySelector('.texttitle').innerText = getCellData('B12');
  })
}

  
function setContent() {

  let i = 0;

  let allHtml = '';

  for (const map of results.maps) {

    const html = `
	<div id="layout_${i}" class="layout off">
		<div class="borderrectangle">
			<div class="progressbar" style="height:${getCellData('C'+(i+2)).replace(',','.').replace(' %', '%')}"><div class="progressfillbar" id="progressfillbar_${i}"></div></div>
		</div>
		<div class="operatorbg">
			<div class="map${i+1}" id="operatorlayout" style="background-image:url('../../GFX/SecurePoints/${getCellData('A'+(i+2))}.png');">
				<div class="op-name">${getCellData('A'+(i+2))}</div>
				<div class="op-line"></div>
				<div class="op-picks">${getCellData('B'+(i+2))} PICKS</div>
				<div class="op-ban">${getCellData('C'+(i+2))}</div>
			</div>
		</div>
	</div>
      `;

    setTimeout(anim(i), 1400+500*i);
	
    allHtml += html;
    i++;
  }

  const container = document.querySelector('.layoutban');
  container.innerHTML = allHtml;
}

function anim(i) {
  return () => {
    document.querySelector(`#layout_${i}`).classList.remove('off');
	document.querySelector(`#progressfillbar_${i}`).style.animation = "percentageup 0.8s 0.2s forwards";
	}
}
