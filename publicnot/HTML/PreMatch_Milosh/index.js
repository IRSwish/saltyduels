let gsheetdata = null;

getGoogleSheetData();

function getCellData(cell) {
	const cellContent = gsheetdata.feed.entry.find((entry) => {
    return entry.title.$t == cell
  });
	return cellContent.content.$t;
}

function getGoogleSheetData(){
	fetch('https://spreadsheets.google.com/feeds/cells/1-9SRClVJkLZ6Y-BgyN4YGcY843gjvtdrHUtBoQU0_NI/4/public/basic?alt=json')
	.then(function(res) {
		return res.json();
  })
  .then((data) => {
    gsheetdata = data;
	document.querySelector('.h1').innerText = getCellData('B1');
	document.querySelector('.logoblue').style.backgroundImage = `url("../../GFX/Photos/` + getCellData('B1') + `.png")`;
	document.querySelector('.logoorange').style.backgroundImage = `url("../../GFX/Photos/` + getCellData('C1') + `.png")`;
	document.querySelector('.h2').innerText = getCellData('C1');
  })
}

setInterval(getGoogleSheetData, 1000);
