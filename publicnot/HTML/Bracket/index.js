let gsheetdata = null;
function getCellData(cell) {
	const cellContent = gsheetdata.feed.entry.find((entry) => {
    return entry.title.$t == cell
  });
	return cellContent.content.$t;
}

function getGoogleSheetData(){
	fetch('https://spreadsheets.google.com/feeds/cells/1-9SRClVJkLZ6Y-BgyN4YGcY843gjvtdrHUtBoQU0_NI/5/public/basic?alt=json')
	.then(function(res) {
		return res.json();
  })
  .then((data) => {
    gsheetdata = data;
	
    // 1ER SEIZIEME DE FINALE
	document.querySelector('.team1name').innerText = getCellData('A2');
	document.querySelector('.team1score').innerText = getCellData('B2');
	document.querySelector('.team2name').innerText = getCellData('A3');
	document.querySelector('.team2score').innerText = getCellData('B3');

    // 2EME SEIZIEME DE FINALE
	document.querySelector('.team3name').innerText = getCellData('A6');
	document.querySelector('.team3score').innerText = getCellData('B6');
	document.querySelector('.team4name').innerText = getCellData('A7');
	document.querySelector('.team4score').innerText = getCellData('B7');
	
    // 3EME SEIZIEME DE FINALE
	document.querySelector('.team5name').innerText = getCellData('A10');
	document.querySelector('.team5score').innerText = getCellData('B10');
	document.querySelector('.team6name').innerText = getCellData('A11');
	document.querySelector('.team6score').innerText = getCellData('B11');
	
    // 4EME SEIZIEME DE FINALE
	document.querySelector('.team7name').innerText = getCellData('A14');
	document.querySelector('.team7score').innerText = getCellData('B14');
	document.querySelector('.team8name').innerText = getCellData('A15');
	document.querySelector('.team8score').innerText = getCellData('B15');
	
    // 5EME SEIZIEME DE FINALE
	document.querySelector('.team9name').innerText = getCellData('A18');
	document.querySelector('.team9score').innerText = getCellData('B18');
	document.querySelector('.team10name').innerText = getCellData('A19');
	document.querySelector('.team10score').innerText = getCellData('B19');
	
    // 6EME SEIZIEME DE FINALE
	document.querySelector('.team11name').innerText = getCellData('A22');
	document.querySelector('.team11score').innerText = getCellData('B22');
	document.querySelector('.team12name').innerText = getCellData('A23');
	document.querySelector('.team12score').innerText = getCellData('B23');
	
    // 7EME SEIZIEME DE FINALE
	document.querySelector('.team13name').innerText = getCellData('A26');
	document.querySelector('.team13score').innerText = getCellData('B26');
	document.querySelector('.team14name').innerText = getCellData('A27');
	document.querySelector('.team14score').innerText = getCellData('B27');
	
    // 8EME SEIZIEME DE FINALE
	document.querySelector('.team15name').innerText = getCellData('A30');
	document.querySelector('.team15score').innerText = getCellData('B30');
	document.querySelector('.team16name').innerText = getCellData('A31');
	document.querySelector('.team16score').innerText = getCellData('B31');
	
    // 9EME SEIZIEME DE FINALE
	document.querySelector('.team17name').innerText = getCellData('A34');
	document.querySelector('.team17score').innerText = getCellData('B34');
	document.querySelector('.team18name').innerText = getCellData('A35');
	document.querySelector('.team18score').innerText = getCellData('B35');
	
    // 10EME SEIZIEME DE FINALE
	document.querySelector('.team19name').innerText = getCellData('A38');
	document.querySelector('.team19score').innerText = getCellData('B38');
	document.querySelector('.team20name').innerText = getCellData('A39');
	document.querySelector('.team20score').innerText = getCellData('B39');
	
    // 11EME SEIZIEME DE FINALE
	document.querySelector('.team21name').innerText = getCellData('A42');
	document.querySelector('.team21score').innerText = getCellData('B42');
	document.querySelector('.team22name').innerText = getCellData('A43');
	document.querySelector('.team22score').innerText = getCellData('B43');
	
    // 12EME SEIZIEME DE FINALE
	document.querySelector('.team23name').innerText = getCellData('A46');
	document.querySelector('.team23score').innerText = getCellData('B46');
	document.querySelector('.team24name').innerText = getCellData('A47');
	document.querySelector('.team24score').innerText = getCellData('B47');
	
    // 13EME SEIZIEME DE FINALE
	document.querySelector('.team25name').innerText = getCellData('A50');
	document.querySelector('.team25score').innerText = getCellData('B50');
	document.querySelector('.team26name').innerText = getCellData('A51');
	document.querySelector('.team26score').innerText = getCellData('B51');
	
    // 14EME SEIZIEME DE FINALE
	document.querySelector('.team27name').innerText = getCellData('A54');
	document.querySelector('.team27score').innerText = getCellData('B54');
	document.querySelector('.team28name').innerText = getCellData('A55');
	document.querySelector('.team28score').innerText = getCellData('B55');
	
    // 15EME SEIZIEME DE FINALE
	document.querySelector('.team29name').innerText = getCellData('A58');
	document.querySelector('.team29score').innerText = getCellData('B58');
	document.querySelector('.team30name').innerText = getCellData('A59');
	document.querySelector('.team30score').innerText = getCellData('B59');
	
    // 16EME SEIZIEME DE FINALE
	document.querySelector('.team31name').innerText = getCellData('A62');
	document.querySelector('.team31score').innerText = getCellData('B62');
	document.querySelector('.team32name').innerText = getCellData('A63');
	document.querySelector('.team32score').innerText = getCellData('B63');

    // 1ER HUITIEME DE FINALE
	document.querySelector('.teamR8_1_name').innerText = getCellData('D4');
	document.querySelector('.teamR8_1_score').innerText = getCellData('E4');
	document.querySelector('.teamR8_2_name').innerText = getCellData('D5');
	document.querySelector('.teamR8_2_score').innerText = getCellData('E5');

	// 2EME HUITIEME DE FINALE
	document.querySelector('.teamR8_3_name').innerText = getCellData('D12');
	document.querySelector('.teamR8_3_score').innerText = getCellData('E12');
	document.querySelector('.teamR8_4_name').innerText = getCellData('D13');
	document.querySelector('.teamR8_4_score').innerText = getCellData('E13');

	// 3EME HUITIEME DE FINALE
	document.querySelector('.teamR8_5_name').innerText = getCellData('D20');
	document.querySelector('.teamR8_5_score').innerText = getCellData('E20');
	document.querySelector('.teamR8_6_name').innerText = getCellData('D21');
	document.querySelector('.teamR8_6_score').innerText = getCellData('E21');

	// 4EME HUITIEME DE FINALE
	document.querySelector('.teamR8_7_name').innerText = getCellData('D28');
	document.querySelector('.teamR8_7_score').innerText = getCellData('E28');
	document.querySelector('.teamR8_8_name').innerText = getCellData('D29');
	document.querySelector('.teamR8_8_score').innerText = getCellData('E29');

	// 5EME HUITIEME DE FINALE
	document.querySelector('.teamR8_9_name').innerText = getCellData('D36');
	document.querySelector('.teamR8_9_score').innerText = getCellData('E36');
	document.querySelector('.teamR8_10_name').innerText = getCellData('D37');
	document.querySelector('.teamR8_10_score').innerText = getCellData('E37');

	// 6EME HUITIEME DE FINALE
	document.querySelector('.teamR8_11_name').innerText = getCellData('D44');
	document.querySelector('.teamR8_11_score').innerText = getCellData('E44');
	document.querySelector('.teamR8_12_name').innerText = getCellData('D45');
	document.querySelector('.teamR8_12_score').innerText = getCellData('E45');

	// 7EME HUITIEME DE FINALE
	document.querySelector('.teamR8_13_name').innerText = getCellData('D52');
	document.querySelector('.teamR8_13_score').innerText = getCellData('E52');
	document.querySelector('.teamR8_14_name').innerText = getCellData('D53');
	document.querySelector('.teamR8_14_score').innerText = getCellData('E53');

	// 8EME HUITIEME DE FINALE
	document.querySelector('.teamR8_15_name').innerText = getCellData('D60');
	document.querySelector('.teamR8_15_score').innerText = getCellData('E60');
	document.querySelector('.teamR8_16_name').innerText = getCellData('D61');
	document.querySelector('.teamR8_16_score').innerText = getCellData('E61');

	// 1ER QUART DE FINALE
	document.querySelector('.teamR4_1_name').innerText = getCellData('G8');
	document.querySelector('.teamR4_1_score').innerText = getCellData('H8');
	document.querySelector('.teamR4_2_name').innerText = getCellData('G9');
	document.querySelector('.teamR4_2_score').innerText = getCellData('H9');
	
	// 2EME QUART DE FINALE
	document.querySelector('.teamR4_3_name').innerText = getCellData('G24');
	document.querySelector('.teamR4_3_score').innerText = getCellData('H24');
	document.querySelector('.teamR4_4_name').innerText = getCellData('G25');
	document.querySelector('.teamR4_4_score').innerText = getCellData('H25');

	// 3EME QUART DE FINALE
	document.querySelector('.teamR4_5_name').innerText = getCellData('G40');
	document.querySelector('.teamR4_5_score').innerText = getCellData('H40');
	document.querySelector('.teamR4_6_name').innerText = getCellData('G41');
	document.querySelector('.teamR4_6_score').innerText = getCellData('H41');

	// 4EME QUART DE FINALE
	document.querySelector('.teamR4_7_name').innerText = getCellData('G56');
	document.querySelector('.teamR4_7_score').innerText = getCellData('H56');
	document.querySelector('.teamR4_8_name').innerText = getCellData('G57');
	document.querySelector('.teamR4_8_score').innerText = getCellData('H57');

	// 1ERE DEMIE FINALE
	document.querySelector('.teamR2_1_name').innerText = getCellData('J16');
	document.querySelector('.teamR2_1_score').innerText = getCellData('K16');
	document.querySelector('.teamR2_2_name').innerText = getCellData('J17');
	document.querySelector('.teamR2_2_score').innerText = getCellData('K17');
	
	// 2EME DEMIE FINALE
	document.querySelector('.teamR2_3_name').innerText = getCellData('J48');
	document.querySelector('.teamR2_3_score').innerText = getCellData('K48');
	document.querySelector('.teamR2_4_name').innerText = getCellData('J49');
	document.querySelector('.teamR2_4_score').innerText = getCellData('K49');

	// FINALE
	document.querySelector('.teamfinale1name').innerText = getCellData('M32');
	document.querySelector('.teamfinale1score').innerText = getCellData('N32');
	
	document.querySelector('.teamfinale2name').innerText = getCellData('M33');
	document.querySelector('.teamfinale2score').innerText = getCellData('N33');
  })
}

getGoogleSheetData();
setInterval(getGoogleSheetData, 1000);
